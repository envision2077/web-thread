/*
 * @Description:
 * @Author: Evan.Zhang
 * @Date: 2022-07-04 14:31:51
 * @LastEditTime: 2022-08-08 16:45:22
 * @LastEditors: Evan.Zhang
 * @Reference:
 */
import { getNoEffectObject} from "./core/lib.js"
import CreateThreadPool from "./core/thread-pool"


/**
 * @description: 抛出异常
 * @return {*}
 * @param {*} Exception
 */
function ThrowException(Exception = new Error("got some error")) {
  throw Exception;
}


const ThreadPoolClass = getNoEffectObject();

// 无限制线程池
ThreadPoolClass.ThreadPool = function (config) {
  config.initSize!== "undefined" && (config.size = config.initSize)
  if (config.size === undefined) {
    config.size = 200
    console.warn("default init size is " + config.size)
  }
  return CreateThreadPool(config);
}



/**
 * 限制线程数的线程池
 *
 * 当无空闲线程 并且线程数量已达设置的线程数量极限，则:
 *
 *    多余的任务将进入 任务队列 等待线程空闲,分配执行;
 */
ThreadPoolClass.LimitedThreadPool = function (config = ThrowException(`CreateThreadPool need config Object!`)) {
  config.initSize!== "undefined" && (config.size = config.initSize)

  const { size, maxSize } = config;
  /**
   * 检查配置项：
   *
   * size: {number} 初始化线程数量
   * maxSize: {number} 线程池数量限制
   */
  if (size === undefined) {
    // 无初始线程数 则 设置线程默认初始化数量
    config.size = 200;
    console.warn("default init size is " + config.size)
  }
  if (maxSize === undefined) {
    // 未设置线程极限数量则 抛出错误
    ThrowException(`CreateThreadPool need config option 'maxSize' to set limit thread quantity!`)
  }
  // 创建线程池
  return CreateThreadPool(config);
}


export default window.ThreadPoolClass = ThreadPoolClass;