/*
 * @Description:
 * @Author: Evan.Zhang
 * @Date: 2022-07-08 19:18:14
 * @LastEditTime: 2022-08-05 16:51:52
 * @LastEditors: Evan.Zhang
 * @Reference:
 */

/**
 * @description: 获取无副作用对象
 * @return {*}
 */
export function getNoEffectObject() {
  return Object.create(null);
}

/**
 * @description: 设置对象原型
 * @return {*}
 * @param {*} obj
 * @param {*} prototype
 */
export function setObjectPrototype(obj, prototype) {
  Object.setPrototypeOf(obj, prototype);
  return obj;
}

/**
 * @description: 字符串多项式哈希
 * @return {*}
 * @param {*} key
 */
export function Hash(key = "10000") {
  const base = 13;
  const SafeInteger = Number.MAX_SAFE_INTEGER - 1;
  let hash = SafeInteger;
  for (
    let charIndex = 0, len = key.length;
    charIndex < len;
    charIndex++
  ) {
    const charCode = key.charCodeAt(charIndex);
    hash = (hash * base + charCode) % SafeInteger;
  }
  return hash.toString(16);
}

// 网络请求文件资源
export function getNetFileSync(url) {
  return new Promise((resolve) => {
    fetch(url).then(response => {
      response.text().then(text => {
        resolve(text)
      });
    })
  })
}