/*
 * @Description:
 * @Author: Evan.Zhang
 * @Date: 2022-08-05 16:13:51
 * @LastEditTime: 2022-08-09 15:34:08
 * @LastEditors: Evan.Zhang
 * @Reference:
 */
import { getNoEffectObject,setObjectPrototype,getNetFileSync } from "../lib"


const WARNING_THREAD_SIZE = 270;
const prototype = getNoEffectObject();

/**
 * @description: 初始化线程池
 * @return {*}
 * @param {*} size
 */
prototype.initPool = function ({ size, maxSize }) {
  this._THREAD_LIMIT_SIZE = maxSize||Infinity;
  this._taskQueue = [];

  if (!size) {
    // 无需线程池初始化线程
    return this
  } else {
    // 需要线程池初始化线程
    const tempPool = [];
    for (let i = 0; i < size; i++) {
      tempPool.push(
        this._getValidWorker()
      )
    }
    this.threadPool.stoppedPool.push(...tempPool);
    return this;
  }
}

/**
 * @description: 将网络JS文件缓存至本地Blob并返回Blob URI
 * @return {*}
 * @param {*} uri
 */
prototype.JsWebLink2BlobLinkAsync = async function (uri) {
  const res = await getNetFileSync(uri);
  const url = URL.createObjectURL(new Blob([res], {
    type: 'text/plain'
  }))
  return url;
}


/**
 * @description: 销毁线程
 * @return {*}
 */
prototype.terminateEachThread = function () {
  this._threadLinerPool.forEach(w=>w&&w.parting())
}

/**
 * @description: 销毁线程并释放所有引用
 * @return {*}
 */
prototype.suicide = function () {
  this.terminateEachThread();
  setObjectPrototype(this, getNoEffectObject());
  Object.keys(this).forEach(key=>delete this[key]);
  delete this;
}


/**
 * @description: 执行代理
 * @return {*}
 * @param {array} rest
 */
prototype.executeByURI = function (...rest) {
  return this._execute("executeByURI",...rest);
}

/**
 * @description: 执行代理
 * @return {*}
 * @param {array} rest
 */
prototype.executeByFunction = function (...rest) {
  return this._execute("executeByFunction",...rest);
}


/**
 * @description: 执行
 * @return {*}
 * @param {*} method
 * @param {array} args
 */
prototype._execute = function (method = "", ...args) {
  const worker = this._getValidWorker();

  if (!worker) {
    // 无可用线程,进入任务队列等待执行
    this._taskQueue.push({method: method,restArgs: args})
    return false;
  } else {
    // 有可用线程,立即执行
    try {
      worker[method](...args);
      this.threadPool.runningPool.push(worker);
    } catch (e) {
      this.threadPool.stoppedPool.push(worker);
      console.error(e)
    }
    return true;
  }
}

/**
 * @description: 检查任务队列是否有任务并尝试执行
 *
 * 若有任务则分配给执行函数（执行函数将确定是否有闲置线程可供使用）
 *
 * 任务队列中的任务优先级不会改变，遵循先入先出，优先分配的原则
 *
 * @return {*}
 * @param {*} worker: 结束运行的空闲线程
 */
prototype.checkTaskQueueAndExecute = function (worker) {
  // 获取任务
  const A_TASK = this._taskQueue.shift();
  // 操作线程分布
  this.threadPool.stoppedPool.push(worker);
  this.threadPool.runningPool.shift();
  if (!A_TASK) {
    // 若任务队列没有任务
    return false
  } else {
    // 若任务队列有任务,交给执行函数
    return this._execute(A_TASK.method, ...A_TASK.restArgs)
  }
}


/**
 * @description: 当线程 【启动,停止】 时
 *
 * 按照线程状态 分配给不同的策略处理
 *
 * @return {*}
 * @param {*} worker
 */
prototype.updateThreadState = function (worker) {
  /**
   * @description: 线程启动策略
   * @return {*}
   */
  function StateTrueTactics() {
    // 线程数目告警
    if (this._threadLinerPool.length >= WARNING_THREAD_SIZE) {
      console.warn(
        "You create so many threads (have trigger out memory errors breakdown risk), please optimize your code."
        ,"\n",
        "创建了过多的线程【增加内存过大崩溃的风险】, 建议优化您的代码."
      );
      // only warning, noting to do.
    }
    return;
  }


  /**
   * @description: 线程停止策略
   * @return {*}
   */
  function StateFalseTactics() {
    // 检查任务队列，将线程优先分配给任务
    this.checkTaskQueueAndExecute(worker);
  }



  // 线程状态更新时的处理策略
  return function (stateFlag) {
    if (stateFlag == true)
      StateTrueTactics.apply(this);
    else
      StateFalseTactics.apply(this);
  }
}



/**
 * @description: 获取一个闲置worker(线程)
 * @return {*}
 */
prototype._getValidWorker = function () {
  let worker = this.threadPool.stoppedPool.shift();
  // 线程够用，返回空闲线程
  if (worker) return worker

  // 达到了线程数量极限，不新开线程
  if(this._threadLinerPool.length >= this._THREAD_LIMIT_SIZE) return undefined;
  // 还未达到线程数量极限,新开线程
  worker = new this.Worker();
  worker.onStateChange = this.updateThreadState(worker).bind(this);
  this._threadLinerPool.push(worker);
  return worker;
}


export default prototype;