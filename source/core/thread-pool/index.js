/*
 * @Description:
 * @Author: Evan.Zhang
 * @Date: 2022-08-05 16:11:52
 * @LastEditTime: 2022-08-08 13:36:49
 * @LastEditors: Evan.Zhang
 * @Reference:
 */

import { setObjectPrototype } from "../lib"


import CreateInstance from "./instance"
import Prototype from "./prototype"


export default function getWThreadPool(config = {
  size: 20,
  maxSize: Infinity
},) {
  // 组合线程池实例
  const ThreadPool = setObjectPrototype(
    CreateInstance(),
    Prototype
  );
  // 初始化线程池
  return ThreadPool.initPool(config);
}