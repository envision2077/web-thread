/*
 * @Description:
 * @Author: Evan.Zhang
 * @Date: 2022-08-05 16:13:42
 * @LastEditTime: 2022-08-05 17:40:24
 * @LastEditors: Evan.Zhang
 * @Reference:
 */
import { getNoEffectObject } from "../lib"
import Worker from "../web-worker"


export default function getNewInstance() {
  const instance = getNoEffectObject();

  instance._threadLinerPool = [];

  instance.threadPool = {
    stoppedPool: [],
    runningPool:[]
  };
  instance.Worker = Worker;

  return instance;
}