# 前端多线程计算库 WebThread 简介

#### 软件代码架构说明

    此计算库 通过包装WebWorker以实现线程池；线程代码将运行在WebWorker作用域，可传入作用域对象，作用域对象将被挂载至this；

    **支持：**

        1. 传入运行时时函数对象；

        2. 传入JS文件URL(运行远程JS文件)

##### 线程中可以使用哪些方法呢？

        参见: [Web Workers 中可以使用的函数和类 - Web API 接口参考 | MDN](https://developer.mozilla.org/zh-CN/docs/Web/API/Web_Workers_API/Functions_and_classes_available_to_workers)

#### 安装教程

```shell
npm install wthread
```

[参考: npm package - wthread](https://www.npmjs.com/package/wthread)

[源码参考: Gitee仓库 - WebThread: 前端web多线程计算库](https://www.npmjs.com/package/wthread)

#### 打包选项

1. yarn run dev
   
   > 开发模式

2. yarn run build:debug
   
   > 开发调试模式

3. yarn run build:prod
   
   > 生产模式

#### 使用示例

引入js文件后将在window上挂载`ThreadPoolClass`对象，对象有两种线程池:

    1. ThreadPool:

> *构造参数: `config : { initSize: <Number> }`*
> 
> 无限制的线程池：当空闲线程不足时，自动创建新线程加入空闲线程队列中，不做线程数量限制与回收（考虑到回收机制受业务不同而影响运行效果，所以不考虑做回收机制）；

    2. LimitedThreadPool:

> *构造参数: `config:{ initSize: <Number>, maxSize:<Number>}`*
> 
> 有限制的线程池：当空闲线程不足时，若线程数量并未达到所设置的极限则创建新线程，若线程数量已达到所设置的极限时，将不会再新增任何线程。
> 
>  若任务队列有待执行任务：优先分配给任务队列中的任务执行，任务执行优先级遵循先入先出的队列顺序；并且：
> 
> **当线程数量到达设置极限且无空闲线程时：**
> 
>  将执行任务加入任务队列。
> 
> **当有线程空闲时：**
> 
>  优先分配给任务队列中的任务执行,若无待执行任务，则进入空闲线程队列待线程池分配；

**线程池使用示例:**

```javascript
// 一，引入
// ======================================================

// npm 获取
import ThreadPoolClass from "wthread";
// -----------------------------------------------------
// script 引入
<script type="text/javascript" src="./bundle.js"></script>

// ======================================================



// 二，获取线程池实例
// ======================================================

// 普通线程池
const threadPool = new ThreadPoolClass.ThreadPool({
    // 初始化线程数量（WebWorker创建有一定的时间/性能开销，建议填写合适的初始化数量）
    initSize:300
})
// -----------------------------------------------------
// 有数量限制的线程池
const threadPool = new ThreadPoolClass.LimitedThreadPool({
    // 初始化线程数量
    initSize:300,
    // 线程数量限制
    maxSize:400
})

// ======================================================



// 三，执行任务
// ======================================================

// 执行本地函数的方式(函数将被序列化传输至WebWorker环境)
function compute(){
    // 离屏渲染
    const bitMap = offscreenCanvas.transferToImageBitmap()
    // 传输结果给线程回调
    postMessage(bitMap)
}

// 执行函数
threadPool.executeByFunction(compute,
// 可选的第二个参数
{
    // 作用域，将被传输并注入至webWorker环境作用域；
    scope:{
        // 一些变量,如：
        offscreenCanvas
    },
    callback:(bitMap)=>{
        // 获得WebWorker的结果数据
        console.log("bitMap>>>",bitMap)
    }
})
// -----------------------------------------------------

// 执行网络JS文件
threadPool.executeByURI("http://xxxxxxx/xxxx.js",
// 可选的第二个参数
{
    // 作用域，将被传输并注入至webWorker环境作用域；
    scope:{
        // 一些变量
    },
    callback:(res)=>{
        // 获得WebWorker的结果数据
        console.log("bitMap>>>",res)
    }
})
// -----------------------------------------------------
// 由于JS文件需要进行http请求因此，可以使用如下方式缓存至本地并生成Blob链接
async function testing(){
   // 获取blob（本地）链接
   const blobURL = await threadPool.JsWebLink2BlobLinkAsync("http://xxxxxxx/xxxx.js")
   // 执行JS文件
   threadPool.executeByURI(blobURL,{
       // 作用域，将被传输并注入至webWorker环境作用域；
       scope:{
           // 一些变量
       },
       callback:(res)=>{
           // 获得WebWorker的结果数据
           console.log("result>>>",res)
       }
   })
   return;
}

// ======================================================


// 四，释放
// ======================================================
// 1,释放所有WebWorker线程,但不释放任何实例相关的引用
threadPool.terminateEachThread();
// 2,释放所有WebWorker线程，并释放自己的相关的引用(实例将不再可用)
threadPool.suicide();
// ======================================================
```

#### 

#### 使用建议

[分配及共享内存 - Web API 接口参考 | MDN](https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/Reference/Global_Objects/SharedArrayBuffer)

[离屏渲染 - Web API 接口参考 | MDN](https://developer.mozilla.org/zh-CN/docs/Web/API/OffscreenCanvas)

[原子操作 - Web API 接口参考 | MDN](https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/Reference/Global_Objects/Atomics)

... ... 更多精彩等你发现 ：）

#### 参与贡献

1. Fork 本仓库
2. 新建 Feat_xxx 分支
3. 提交代码
4. 新建 Pull Request
