/*
 * @Description:
 * @Author: Evan.Zhang
 * @Date: 2022-07-04 14:31:04
 * @LastEditTime: 2022-08-05 16:50:24
 * @LastEditors: Evan.Zhang
 * @Reference:
 */

import { uglify } from "rollup-plugin-uglify";
import preParser from "./rollup-plugin/pre-parser"
import resolve from 'rollup-plugin-node-resolve';
import babel from 'rollup-plugin-babel';
export default {
  input: 'source/index.js',
  output:{
    name:'rollup',
    file: 'dist/bundle.js',
    format: 'umd',
  },
  plugins: process.env.ENV === 'production' ?
  [
    resolve(),
    babel({
      exclude: 'node_modules/**'
    }),
    preParser(),
    uglify(),
  ]
  :
  [
    resolve(),
    babel({
      exclude: 'node_modules/**'
    }),
    preParser(),
  ]
}